/**
 * Created by idaliya on 29.02.16.
 */

const ONE_MIN_PIX = 1.5;
require('whatwg-fetch');

//json where all info about lessons
var url = './test.json';

var getPopupStatusClassList = function (status, classList) {
    if (!status) {
        classList = 'calendar-week--popup__normal calendar-week--popup__arrow-normal ';
    } else if (status == 'cancel') {
        classList = 'calendar-week--popup__cancel calendar-week--popup__arrow-cancel '
    }
    else if (status == 'changed') {
        classList = 'calendar-week--popup__changed calendar-week--popup__arrow-changed '
    }
    return classList;
};

var getPopupOffsetPosition = function (lesson) {
    var elem = lesson.parentNode;
    var middlePosition = (130 * 7) / 2;
    if (elem.offsetLeft < middlePosition) {
        return 'calendar-week--popup__arrow-left '
    } else return 'calendar-week--popup__arrow-right '
};

var setPopupTopOffset = function (lesson, elem, popup) {
    var lessonTopOffset = lesson.offsetTop;
    var weekHeight = lesson.parentNode.parentNode.offsetHeight;
    var popupOffset = popup.parentNode.offsetTop + lessonTopOffset;
    if (popupOffset + popup.offsetHeight > weekHeight) {
        var top = lessonTopOffset + lesson.offsetHeight - popup.offsetHeight;

        popup.classList.add('calendar-week--popup__arrow-bottom');
    }
    else {
        var top = lessonTopOffset;
        popup.classList.add('calendar-week--popup__arrow-top');
    }

    popup.style.top = top + 'px';

};

var getPopupHtml = function(elem, popup){

    var minFrom = elem.time.from.min;
    var minTo = elem.time.to.min;

    if (minFrom < 10) minFrom = '0' + minFrom;
    if (minTo < 10) minTo = '0' + minTo;

    var headerInfo, changedInfo, timeInfo, theme, hometask, cabinet = '';

        if (elem.lastGender) {
        headerInfo = ` <div class="calendar-week--popup-header-text-item strikeout-text">(${elem.lastGender})</div> `;
    }

    if (elem.gender) {
        headerInfo = ` <div class="calendar-week--popup-header-text-item">(${elem.gender})</div> ` + headerInfo;
    }

    if (elem.lastTime) {
        timeInfo = ` <div class="calendar-week--popup-header-time-item strikeout-text">(${elem.lastTime})</div> `
    }

    if (elem.lessonTheme) {
        theme = `<div class="calendar-week--popup--lesson-theme">
                    <div class="calendar-week--popup--lesson-theme-header">Тема урока</div>
                    <div class="calendar-week--popup--lesson-theme-item">${elem.lessonTheme}</div>
                </div>
        `;
    }

    if (elem.hometask) {
        hometask = `<div class="calendar-week--popup-hometask">
                    <div class="calendar-week--popup-hometask-header">Дом. задание</div>`
            elem.hometask.forEach(function(homeTask){
                hometask = hometask + `<div class="calendar-week--popup-hometask-item__normal calendar-week--popup-hometask-item">${elem.hometask}</div>`
            });
            hometask = hometask +  `</div>`;
    }

    if (elem.cabinet) {
        cabinet = `
                <div class="calendar-week--popup-cabinet">
                    <div class="calendar-week--popup-cabinet-header">Кабинет:</div>
                    <div class="calendar-week--popup-cabinet-item">${elem.cabinet}</div>
                </div>
        `;
    }

    if (elem.changedInfo) {
        changedInfo = `<div class="calendar-week--popup-changed-info">
                        <div class="calendar-week--popup-changed-info-item">${elem.changedInfo}</div>
                    </div>`;
    }


    return popup.innerHTML =
        `
            <div class="calendar-week--popup-header">
                <div class="calendar-week--popup-header-info">

                    <div class="calendar-week--popup-header-text">
                        <div class="calendar-week--popup-header-text-item">${elem.lesson}</div>
                        ${headerInfo || ''}
                    </div>
                    <div class="calendar-week--popup-header-time">
                        ${timeInfo || ''}
                        <div class="calendar-week--popup-header-time-item">${elem.time.from.hour || ''}:${minFrom || ''}-${elem.time.to.hour || ''}:${minTo || ''}</div>
                    </div>
                </div>
                <a class="calendar-week--popup-header-right"></a>
            </div>
            ${theme || ''}
            ${hometask || ''}
            ${cabinet || ''}
            ${changedInfo || ''}
            <div class="calendar-week--popup-close">
                <a href="" class="calendar-but calendar-week--popup-close-item">Закрыть</a>
            </div>
        `;
};

var setPopup = function (popup, lesson, elem, gridHeight) {

    var classList = getPopupStatusClassList(elem.status, ' ');
    classList = classList + getPopupOffsetPosition(lesson) + 'calendar-week--popup calendar-week--popup__arrow';
    var arrayOfClassList = classList.split(' ');
    popup.classList.add.apply(popup.classList, arrayOfClassList);

    popup.innerHTML = getPopupHtml(elem ,popup);

    lesson.parentNode.insertBefore(popup, lesson);
    setPopupTopOffset(lesson, elem, popup);
};

var getMinMaxHour = function (week) {
    var minHour = week[0].lessons[0].time.from.hour;
    var maxHour = week[0].lessons[0].time.from.hour;
    Array.prototype.forEach.call(week, function (elem) {
        if (elem.lessons) {
            Array.prototype.forEach.call(elem.lessons, function (lesson) {
                if (lesson.time.to.hour > maxHour) {
                    maxHour = lesson.time.to.hour;
                }
                if (lesson.time.from.hour < minHour) {
                    minHour = lesson.time.from.hour;
                }

            })
        }

    });
    return [minHour, maxHour];
};

var setLessonsCountInfo = function (normal, cancel, changed) {
    var lessonCountInfo = document.getElementsByClassName('calendar-week--lessons-info-val')[0];
    lessonCountInfo.innerText = ` всего ${normal + cancel + changed} уроков по плану, ${changed} замена, ${changed} отмен.`;
};

var createGrid = function (hoursInDay, hoursCount) {
    var calendarWeek = document.getElementsByClassName('calendar-week--hours-wrap')[0];
    calendarWeek.style.height = hoursInDay * 60 * ONE_MIN_PIX + 'px';

    for (var i = 0; i < hoursCount; i++) {
        var hour = document.createElement('div');
        hour.classList.add('calendar-week--hour');
        calendarWeek.appendChild(hour);
    }
};

var setDayStatus = function (status, dayCount, hoursInDay) {
    var timeBlocks = document.getElementsByClassName('calendar-week--hour');
    var dayFrom = hoursInDay * dayCount;
    if (status == 'today') {
        for (var i = dayFrom; i < dayFrom + hoursInDay; i++) {
            timeBlocks[i].classList.add('calendar-week--hour__today');
        }
    }
    if (status == 'weekend') {
        var textPosition = Math.ceil(hoursInDay / 2);
        var k = 0;
        for (var i = dayFrom; i < dayFrom + hoursInDay; i++) {
            timeBlocks[i].classList.add('calendar-week--hour__weekend');
            k++;
            if (k == textPosition) {

                var text = document.createElement('div');
                text.classList.add('calendar-week--hour__weekend-text');
                text.innerText = 'Праздничный день';
                timeBlocks[i].appendChild(text);
            }
        }
    }
    if (status == 'today-weekend') {
        var textPosition = Math.ceil(hoursInDay / 2);
        var k = 0;
        for (var i = dayFrom; i < dayFrom + hoursInDay; i++) {
            timeBlocks[i].classList.add('calendar-week--hour__today-weekend');
            k++;
            if (k == textPosition) {

                var text = document.createElement('div');
                text.classList.add('calendar-week--hour__weekend-text');
                text.innerText = 'Праздничный день';
                timeBlocks[i].appendChild(text);
            }
        }
    }
};

var setLessonHeight = function (lesson, time) {
    var height = ((time.to.hour * 60 + time.to.min) - (time.from.hour * 60 + time.from.min)) * ONE_MIN_PIX;
    if (height < 65) {
        height = 65;
    }
    lesson.style.height = height + 'px';
};

var setLessonInfo = function (lesson, time, elem) {
    var minFrom = time.from.min;
    var minTo = time.to.min;
    if (minFrom < 10) minFrom = '0' + minFrom;
    if (minTo < 10) minTo = '0' + minTo;
    lesson.innerHTML = `
                <div class= 'calendar-week--lesson-time'> ${time.from.hour}:${minFrom}-${time.to.hour}:${minTo}</div>
                <div class="calendar-week--lesson-name">${elem.lesson}</div>
                <div class="calendar-week--lesson-cabinet">Каб.${elem.cabinet}</div>
            `;

};

var setDaysTitle = function (dayText, dayValue, currentDay, today) {
    if (today) {
        document.getElementsByClassName('calendar-week--days-item-val')[currentDay - 1].innerText = dayValue;
        document.getElementsByClassName('calendar-week--days-item-text')[currentDay - 1].innerText += " (cегодня)";
        today.classList.add('calendar-week--days-item__today');
    }
    else dayText.innerText = dayValue + ' ' + dayText.innerText;
};

var setLessonToPosition = function (lesson, time, i, startHour, hoursInDay) {
    var timeBlocks = document.getElementsByClassName('calendar-week--hour');
    var hourPosition = time.from.hour - startHour;
    var topPosition = time.from.min * ONE_MIN_PIX;
    lesson.style.top = topPosition + 'px';
    timeBlocks[(i * hoursInDay) + hourPosition].appendChild(lesson);
};

var setLessonStatus = function (lesson, status) {
    if (status == 'changed') {
        lesson.classList.add('calendar-week--lesson__changed');
    } else if (status == 'cancel') {
        lesson.classList.add('calendar-week--lesson__cancel');
    } else {
        lesson.classList.add('calendar-week--lesson__normal');
    }
};

var setLessons = function (lesson, elem, i, startHour, hoursInDay) {
    var status = elem.status || null;
    lesson.classList.add('calendar-week--lesson');
    setLessonStatus(lesson, status);
    setLessonHeight(lesson, elem.time);
    setLessonInfo(lesson, elem.time, elem);
    setLessonToPosition(lesson, elem.time, i, startHour, hoursInDay);
};

var createLessons = function (daysArray, startHour, hoursInDay) {
    var daysTitlesVal = document.getElementsByClassName('calendar-week--days-item-val');
    var daysTitles = document.getElementsByClassName('calendar-week--days-item');
    var normalDayCount = 0;
    var cancelDayCount = 0;
    var changedDayCount = 0;
    Array.prototype.forEach.call(daysArray, function (item, i) {

        if (item.status && (item.status.indexOf('today') + 1)) {
            setDaysTitle(daysTitlesVal[i], item.day, i, daysTitles[i]);
        } else setDaysTitle(daysTitles[i], item.day);

        setDayStatus(item.status, i, hoursInDay);
        if (item.lessons) {
            Array.prototype.forEach.call(item.lessons, function (elem) {
                if (!elem.status) normalDayCount++;
                if (elem.status == 'cancel') cancelDayCount++;
                if (elem.status == 'changed') changedDayCount++;
                var lesson = document.createElement('a');
                var popup = document.createElement('div');
                lesson.setAttribute('href', ' ');
                setLessons(lesson, elem, i, startHour, hoursInDay);
                setPopup(popup, lesson, elem, hoursInDay * ONE_MIN_PIX * 60);

                lesson.addEventListener('click', function(event) {
                    event.preventDefault();
                    var popups = document.getElementsByClassName('calendar-week--popup');
                    var lessons = document.getElementsByClassName('calendar-week--lesson');
                    Array.prototype.forEach.call(popups, function(popup, i) {
                        popup.classList.remove('calendar-week--popup__visible');
                        lessons[i].classList.remove('calendar-week--lesson__active');
                    });
                    popup.classList.add('calendar-week--popup__visible');
                    lesson.classList.add('calendar-week--lesson__active')
                });

                Array.prototype.forEach.call(popup.childNodes, function(popupElem) {
                    if(popupElem.classList && popupElem.classList.contains('calendar-week--popup-close')) {

                        popupElem.addEventListener('click', function(event) {
                            event.preventDefault();
                            popup.classList.remove('calendar-week--popup__visible');
                        });
                    }
                })

            })
        }
    });

    setLessonsCountInfo(normalDayCount, cancelDayCount, changedDayCount);
};

var createWeek = function (week) {
    var minMaxHourArray = getMinMaxHour(week);

    var minHour = minMaxHourArray[0];
    var maxHour = minMaxHourArray[1];
    var hoursInDay = maxHour - minHour + 1;
    var startHour = minHour;
    var hoursCount = hoursInDay * 7;

    createGrid(hoursInDay, hoursCount);

    createLessons(week, startHour, hoursInDay);



    var createHoursPanel = function (hoursValue, startHour) {
        var currentHour = startHour;
        for (var i = startHour; i < hoursValue + startHour; i++) {
            var hour = document.createElement('li');
            hour.classList.add('calendar-week--hours-item');
            hour.innerText = currentHour + ':00';
            currentHour++;
            document.getElementsByClassName('calendar-week--hours')[0].appendChild(hour);
        }
    }(hoursInDay, startHour);
};

var getWeekInfo = function (url) {
    fetch(url)
        .then(function (response) {
            if (response.status !== 200) {
                var errMessage = 'Looks like there was a problem. Status Code: ' + response.status;
                throw new Error(errMessage);
            }

            return response.json();
        })
        .catch(function (err) {
            console.log('Fetch Error :-S', err);
        })
        .then(createWeek);
};

document.addEventListener('DOMContentLoaded', function addLessons() {

    getWeekInfo(url);


}, false);
