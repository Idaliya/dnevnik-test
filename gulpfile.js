var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var bulkSass = require('gulp-sass-bulk-import');
var browserify = require('browserify');
var babelify = require('babelify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('jade', function() {
    var YOUR_LOCALS = {};

    gulp.src('./src/*.jade')
        .pipe(jade({
            locals: YOUR_LOCALS
        }))
        .pipe(gulp.dest('./dist/'))
});

gulp.task('sass', function() {
    return gulp
        .src('./src/**/main.scss')
        .pipe(bulkSass())
        .pipe(
            sass({
                includePaths: ['./node_modules','./src/blocks']
            }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe( gulp.dest('./dist/') );
});


gulp.task('javascript', function () {
    return browserify({ entries: './src/main.js' })
        .transform('babelify' , { presets: ["es2015"] })
        .bundle()
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(gulp.dest('./dist/'));
});


gulp.task('sass:watch', function () {
    gulp.watch('./src/**/*.scss', ['sass']);
});

gulp.task('javascript:watch', function () {
    gulp.watch('./src/main.js', ['javascript']);
});

gulp.task('jade:watch', function () {
    gulp.watch('./src/**/*.jade', ['jade']);
});

gulp.task('images', function() {
    return gulp.src(['src/**/*.png', 'src/**/*.jpg'])
        .pipe(gulp.dest('dist'));
});

gulp.task('default',  ['sass','jade','images','javascript','sass:watch','jade:watch','javascript:watch' ] );